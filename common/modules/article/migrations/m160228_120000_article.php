<?php

use yii\db\Schema;
use yii\db\Migration;

class m160228_120000_article extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%article_category}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(1024)->notNull(),
            'title' => $this->string(512)->notNull(),
            'body' => $this->text(),
            'parent_id' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(1024)->notNull(),
            'title' => $this->string(512)->notNull(),
            'body' => $this->text()->notNull(),
            'category_id' => $this->integer(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'published_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->insert('{{%article}}', [
            'id'=>1,
            'slug'=>'article-1',
            'title'=>'article 1',
            'body'=>'some content 1',
            'status'=>\common\modules\article\models\Article::STATUS_PUBLISHED,
            'published_at' => time(),
            'created_at'=>time(),
            'updated_at'=>time()
        ]);

        $this->insert('{{%article}}', [
            'id'=>2,
            'slug'=>'article-2',
            'title'=>'article 2',
            'body'=>'some content 2',
            'status'=>\common\modules\article\models\Article::STATUS_PUBLISHED,
            'published_at' => time(),
            'created_at'=>time(),
            'updated_at'=>time()
        ]);

        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'content' => $this->text()->notNull(),
            'article_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%article}}');
        $this->dropTable('{{%article_category}}');
    }
}
