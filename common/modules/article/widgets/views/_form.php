<?php

/**
 * Comments widget form view.
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \common\modules\article\models\Comment $model New comment model
 */

use yii\helpers\Html;

?>
<?= Html::beginForm(['/article/comment/create'], 'POST', ['class' => 'form-horizontal', 'data-comment' => 'form', 'data-comment-action' => 'create']) ?>
    <div class="form-group" data-comment="form-group">
        <div class="col-sm-12">
            <?= Html::activeTextarea($model, 'content', ['class' => 'form-control']) ?>
            <?= Html::error($model, 'content', ['data-comment' => 'form-summary', 'class' => 'help-block hidden']) ?>
        </div>
    </div>
<?= Html::activeHiddenInput($model, 'parent_id', ['data-comment' => 'parent-id']) ?>
<?= Html::activeHiddenInput($model, 'article_id') ?>
<?= Html::submitButton('Add Comment', ['class' => 'btn btn-danger btn-lg']); ?>
<?= Html::endForm(); ?>