<?php

/**
 * Comments item view.
 *
 * @var \yii\web\View $this View
 * @var \common\modules\article\models\Comment[] $models Comments models
 */

use yii\helpers\Url;

?>
<?php if ($models !== null) : ?>
    <?php foreach ($models as $comment) : ?>
        <div class="media" data-comment="parent" data-comment-id="<?= $comment->id ?>" style="padding-left: <?= intval($comment->level * 20)?>px">
            <div class="media-body">
                <div class="well" data-comment="append">
                    <div class="media-heading">
                        <strong><?= $comment->author->username ?></strong>&nbsp;
                        <small><?= $comment->created ?></small>
                        <?php if ($comment->parent_id) { ?>
                            &nbsp;
                            <a href="#comment-<?= $comment->parent_id ?>" data-comment="ancor" data-comment-parent="<?= $comment->parent_id ?>"><i class="icon-arrow-up"></i></a>
                        <?php } ?>
                        <div class="pull-right" data-comment="tools">
                            <?php if (!Yii::$app->user->isGuest) { ?>
                                <a href="#" data-comment="reply" data-comment-id="<?= $comment->id ?>">
                                    <i class="icon-repeat"></i> Reply
                                </a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="content" data-comment="content"><?= strip_tags($comment->content, '<p><a>') ?></div>

                </div>
                <?php if ($comment->children) { ?>
                    <?= $this->render('_index_item', ['models' => $comment->children]) ?>
                <?php } ?>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>