<?php

/**
 * Comments list view.
 *
 * @var \yii\web\View $this View
 * @var \common\modules\article\models\Comment[] $models Comments models
 * @var \common\modules\article\models\Comment $model New comment model
 */

?>

<div id="comments">
    <div id="comments-list" data-comment="list">
        <?= $this->render('_index_item', ['models' => $models]) ?>
    </div>
    <!--/ #comments-list -->

    <?php if (!Yii::$app->user->isGuest) : ?>
        <h3>Add New Comment</h3>
        <?= $this->render('_form', ['model' => $model]); ?>
    <?php endif; ?>
</div>