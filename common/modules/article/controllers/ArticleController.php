<?php

namespace common\modules\article\controllers;

use common\modules\article\models\Article;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ArticleController extends Controller{

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider(
            [
                'query'=>Article::find()->published()->orderBy(['created_at'=>SORT_DESC])
            ]
        );
        return $this->render('index', ['dataProvider'=>$dataProvider]);
    }

    public function actionView($id)
    {
        return $this->render(
            'view',
            ['model' => $this->loadArticle($id)]
        );
    }

    public function actionComment($id)
    {
        $article = $this->loadArticle($id);
    }

    protected function loadArticle($id)
    {
        $model = Article::find()->published()->andWhere(['id'=>$id])->one();
        if(!$model){
            throw new NotFoundHttpException;
        }
        return $model;
    }
}
