<?php

namespace common\modules\article;

use indigerd\migrationaware\MigrationAwareInterface;

class Module extends \yii\base\Module implements MigrationAwareInterface
{
    public $controllerNamespace = 'common\modules\article\controllers';

    public function getMigrationPath()
    {
        return __DIR__.'/migrations';
    }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}