<?php
/* @var $this yii\web\View */
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <h1><?= $model->title ?></h1>
    <?= $model->body ?>
    <?php
        echo \common\modules\article\widgets\Comments::widget(
            [
                'model' => $model,
                'jsOptions' => [
                    'listSelector' => '[data-comment="list"]', // Comment list selector
                    'parentSelector' => '[data-comment="parent"]', // Comment parent selector
                    'appendSelector' => '[data-comment="append"]', // Container selector where "reply" and "edit" form will be appended by jQuery
                    'formSelector' => '[data-comment="form"]', // Comment form selector
                    'contentSelector' => '[data-comment="content"]', // Comment content selector
                    'toolsSelector' => '[data-comment="tools"]', // Comment tools selector
                    'formGroupSelector' => '[data-comment="form-group"]', // Comment form group selector
                    'errorSummarySelector' => '[data-comment="form-summary"]', // Comment form summary error selector
                    'errorSummaryToggleClass' => 'hidden', // Comment summary error class that will be add/remove by jQuery on error reporting
                    'errorClass' => 'has-error', // Comment form group error class
                    'offset' => 0 // Top offset for scrollTo function. Use it if you have fixed top menu for correct scrolling to comment's parent. In case with fixed menu, "offset" value must be equal with menu block height.
                ]
            ]
        );
    ?>
</div>