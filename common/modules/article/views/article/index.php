<?php
/* @var $this yii\web\View */
$this->title = 'Articles'
?>
<div id="article-index">
    <h1>Articles</h1>
    <?= \yii\widgets\ListView::widget([
        'dataProvider'=>$dataProvider,
        'pager'=>[
            'hideOnSinglePage'=>true,
        ],
        'itemView'=>'_item'
    ])?>
</div>