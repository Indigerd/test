<?php

namespace common\modules\article\models\query;

use common\modules\article\models\Article;
use yii\db\ActiveQuery;

class ArticleQuery extends ActiveQuery {
    public function published()
    {
        $this->andWhere(['status' => Article::STATUS_PUBLISHED]);
        $this->andWhere('article.published_at < NOW()');
        return $this;
    }
}
