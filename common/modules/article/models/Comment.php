<?php
namespace common\modules\article\models;

use common\models\User;
use common\modules\article\models\Article;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%comments}}".
 *
 * @property integer $id ID
 * @property integer $article_id Article ID
 * @property integer $author_id Author ID
 * @property string $content Content
 * @property integer $created_at Create time
 * @property integer $updated_at Update time
 *
 * @property \common\models\User $author Author
 * @property Article $article Article
 */
class Comment extends ActiveRecord
{
    /**
     * @var null|array|\yii\db\ActiveRecord[] Comment children
     */
    protected $_children;

    /**
     * @var string Created date
     */
    private $_created;
    /**
     * @var string Updated date
     */
    private $_updated;
    /**
     * @return string Created date
     */
    public function getCreated()
    {
        if ($this->_created === null) {
            $this->_created = Yii::$app->formatter->asDate($this->created_at, 'd LLL Y');
        }
        return $this->_created;
    }
    /**
     * @return string Updated date
     */
    public function getUpdated()
    {
        if ($this->_updated === null) {
            $this->_updated = Yii::$app->formatter->asDate($this->updated_at, 'd LLL Y');
        }
        return $this->_updated;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class'=>BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',

            ],
        ];
    }

    /**
     * $_children getter.
     *
     * @return null|array|]yii\db\ActiveRecord[] Comment children
     */
    public function getChildren()
    {
        return $this->_children;
    }

    /**
     * $_children setter.
     *
     * @param array|\yii\db\ActiveRecord[] $value Comment children
     */
    public function setChildren($value)
    {
        $this->_children = $value;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Require
            ['content', 'required'],
            // Parent ID
            [
                'parent_id',
                'exist',
                'targetAttribute' => 'id',
                'filter' => ['article_id' => $this->article_id]
            ],
            // Content
            ['content', 'string']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * Get comments tree.
     *
     * @param integer $article_id article ID
     *
     * @return array|\yii\db\ActiveRecord[] Comments tree
     */
    public static function getTree($article_id)
    {
        $models = self::find()->where([
            'article_id' => $article_id,
        ])->orderBy(['parent_id' => 'ASC', 'created_at' => 'ASC'])->with(['author'])->all();
        if ($models !== null) {
            $models = self::buildTree($models);
        }
        return $models;
    }

    public $level = 0;

    /**
     * Build comments tree.
     *
     * @param array $data Records array
     * @param int $rootID parent_id Root ID
     *
     * @return array|\yii\db\ActiveRecord[] Comments tree
     */
    protected static function buildTree(&$data, $rootID = 0, $level = 0)
    {
        $tree = [];
        foreach ($data as $id => $node) {
            if ($node->parent_id == $rootID) {
                unset($data[$id]);
                $node->children = self::buildTree($data, $node->id, $level + 1);
                $node->level = $level;
                $tree[] = $node;
            }
        }
        return $tree;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'create' => ['parent_id', 'article_id', 'content'],
            'update' => ['content'],
        ];
    }
}
